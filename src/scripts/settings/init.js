// shorthands for common elements on the interface
var $signOut, $importer, $exporter;

if (!chrome.cookies) {
  console.log('experimental loaded');
  chrome.cookies = chrome.experimental.cookies;
}

document.addEventListener('DOMContentLoaded', function() {
  
  $signOut = document.getElementById('signout');
  $importer = document.getElementById('importer');
  $exporter = document.getElementById('exporter');

  $importer.href = API_URL + '/import'
  $exporter.href = API_URL + '/export'

  chrome.cookies.get({ "name": "koa.sid", "url": API_URL }, function(cookies) {
    bindActionsToEvents();

    if(!cookies) {
      window.location = 'signin.html';
    }
  });

});
