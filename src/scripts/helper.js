/**
 * Builds and returns a shortcut component
 *
 * @param {object} mark - The mark whose data is used to build
 *   the shortcut.
 **/
function buildShortcut(mark, callback) {

  var shortcut = document.createElement('div');
  shortcut.dataset.id = mark.id;
  shortcut.dataset.name = mark.name;
  shortcut.dataset.url = mark.url;
  shortcut.className = 'shortcut';

  var img = document.createElement('img');
  img.src = "data:image/png;base64," + mark.favicon;
  img.alt = ".";

  var link = document.createElement('a');
  link.href = mark.url;
  link.rel = 'noreferrer';
  link.onclick = openLink;
  link.tabIndex = -1;
  link.dataset.keycode = mark.shortcut;

  var command = document.createElement('span');
  command.className = 'command';
  command.textContent = String.fromCharCode(mark.shortcut)
                              .toLowerCase();

  shortcut.appendChild(command);
  shortcut.appendChild(link);
  link.appendChild(img);

  callback(shortcut);

}

/**
 * Builds and returns a search result
 *
 * @param {object} mark - The mark whose data is used to build
 *   the search result.
 **/
function buildSearchResult(mark, callback) {
  var result = document.createElement('li');
  result.dataset.id = mark.id;
  result.dataset.name = mark.name;
  result.dataset.url = mark.url;
  result.dataset.pinned = mark.pinned;
  result.dataset.shortcut = mark.shortcut;
  result.dataset.favicon = mark.favicon;

  var resultContainer = document.createElement('div');

  var img = document.createElement('img');
  img.src = "data:image/png;base64," + mark.favicon;
  img.alt = ".";

  var bookmark = document.createElement('a');
  bookmark.href = mark.url;
  bookmark.innerText = mark.name;
  bookmark.rel = 'noreferrer';
  bookmark.onclick = openLink;
  bookmark.className  = 'search-result';
  bookmark.onfocus = function() {
    $mode.textContent = mark.url;
  };

  var bookmarkEdit = document.createElement('div');
  bookmarkEdit.className = 'dropdown';

  bookmarkEdit.appendChild(buildDropdownButton());
  bookmarkEdit.appendChild(buildDropdownList());

  /* bookmarkEdit.innerHTML = '<button class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> \
                              <span class="plus">+</span> \
                              <span class="minus">-</span> \
                            </button> \
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1"> \
                              <li><a href="#" class=".edit">Edit</a></li> \
                              <li><a href="#" class=".edit">Delete</a></li> \
                            </ul>'; */

  result.appendChild(resultContainer);
  resultContainer.appendChild(img);
  resultContainer.appendChild(bookmark);
  resultContainer.appendChild(bookmarkEdit);

  callback(result);

}

function buildDropdownButton() {
  var button = document.createElement('button');
  button.className = 'dropdown-toggle';
  button.type = 'button';
  button.dataset.toggle = 'dropdown';
  button.setAttribute('aria-haspopup', true);
  button.setAttribute('aria-expanded', false);

  var plus = document.createElement('span');
  plus.className = 'plus';
  plus.innerText = '+';

  var minus = document.createElement('span');
  minus.className = 'minus';
  minus.innerText = '-';

  button.appendChild(plus);
  button.appendChild(minus);

  return button;
}

function buildDropdownList() {
  var list = document.createElement('ul');
  list.className = 'dropdown-menu dropdown-menu-right';

  var editLi = document.createElement('li');
  var editA = document.createElement('a');
  editA.onclick = clickEditClass;
  editA.href = '#';
  editA.innerText = 'Edit';
  editLi.appendChild(editA);

  var deleteLi = document.createElement('li');
  var deleteA = document.createElement('a');
  deleteA.onclick = clickDeleteClass;
  deleteA .href = '#';
  deleteA.innerText = 'Delete';
  deleteLi.appendChild(deleteA);

  list.appendChild(editLi);
  list.appendChild(deleteLi);

  return list;
}

//function setupEditModal(data, callback) {
//
//  $('#delete-favicon')[0].src = "data:image/png;base64," + data.favicon;
//  $('#delete-name')[0].innerText = data.name;
//  $('#delete-url')[0].innerText = data.url;
//  $('#delete-url')[0].href = data.url;
//
//  //callback(result);
//
//}

//function setupDeleteModal(data, callback) {


  //callback(result);

//}

/**
 * Overrides how a link is opened so that if the open tab is a 'New Tab' the
 * link is opened in that tab. But if the open tab is not a 'New Tab' then the
 * link is opened in a new tab.
 *
 * @param {object} event - The click event attempting to open the link.
 *
 * @param {string} tabAddress - The url of the open tab
 *
 **/
function openLink(event, tabAddress) {
  event.preventDefault();

  var url;

  if (this.href) url = this.href;
  else url = tabAddress;

  getCurrentTab(function(tab) {
    if (tab.url === 'chrome://newtab/') {
      chrome.tabs.update(tab.id, { url: url });
      window.close();
    } else {
      chrome.tabs.create({ url: url });
    }
  });
}


/**
 * See: http://jaketrent.com/post/addremove-classes-raw-javascript/
 * Checks if an element has a given CSS class already
 *
 * @param {object} ele - The element which is being checked.
 *
 * @param {object} cls - The class being checked for.
 *
 **/
function hasClass(ele,cls) {
  return !!ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}

/**
 * See: http://jaketrent.com/post/addremove-classes-raw-javascript/
 * Adds a CSS class to a given element
 *
 * @param {object} ele - The element which is having the
 * class added to it.
 *
 * @param {object} cls - The class being added.
 *
 **/
function addClass(ele,cls) {
  if (!hasClass(ele,cls)) ele.className += " "+cls;
}

/**
 * See: http://jaketrent.com/post/addremove-classes-raw-javascript/
 * Removes a CSS class from a given element
 *
 * @param {object} ele - The element which is having the
 * class removed from it.
 *
 * @param {object} cls - The class being removed.
 *
 **/
function removeClass(ele,cls) {
  if (hasClass(ele,cls)) {
    var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
    ele.className=ele.className.replace(reg,' ');
  }
}

function deleteCookies(callback) {
  chrome.cookies.remove({ "name": "koa.sid", "url": API_URL }, callback);
  chrome.cookies.remove({ "name": "koa.sid.sig", "url": API_URL }, callback);
}
