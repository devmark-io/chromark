$(document).ready(function() {

  $('#signin-form').formValidation({
    // validating Bootstrap form
    framework: 'bootstrap',

    //err: {
      //container: '#messages'
    //},

    // Feedback icons
    icon: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },

    // List of fields and their validation rules
    fields: {
      signin: {
        validators: {
          notEmpty: {
            message: 'Please enter your username or email'
          }
        }
      },
      password: {
        validators: {
          notEmpty: {
            message: 'Please enter your password'
          },
          blank: {}
        }
      }
    }
  })
  // for the sign in we don't show the success visuals since everything
  // is a success other than having a blank input
  .on('success.field.fv', function(e, data) {
    var $parent = data.element.parents('.form-group');

    // Remove the has-success class
    $parent.removeClass('has-success');

    // Hide the success icon
    data.element.data('fv.icon').hide();
  })
  .on('success.form.fv', function(e) {
    // Prevent form submission
    e.preventDefault();

    var $form = $(e.target),
        fv = $form.data('formValidation'),
        url = API_URL + '/account/signin',
        method = 'POST';

    // Use Ajax to submit form data
    $.ajax({
      url: url,
      type: method,
      data: $form.serialize(),
      beforeSend: function() {
        $('#signin-spinner')[0].style.visibility = 'visible';
        $('#messages').html('');
      },
      success: function(result) {
        window.location = 'main.html';
      },
      complete: function(result) {
        $('#signin-spinner')[0].style.visibility = 'hidden';
      },
      error: function(result) {
        if (result.status === 0) {
          window.location = 'errors/connection-error.html';
        }
        else {
          $('#messages').html('<small class="help-block" >' + result.responseText + '</small>');
        }
      }
    });
  });

  $('#register-form').formValidation({
    // validating Bootstrap form
    framework: 'bootstrap',

    // Feedback icons
    icon: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },

    // List of fields and their validation rules
    fields: {
      username: {
        validators: {
          notEmpty: {
            message: 'Please enter a username'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'Please enter your email address'
          },
          emailAddress: {
            message: 'That is not a valid email address'
          }
        }
      },
      password: {
        validators: {
          notEmpty: {
            message: 'Please enter your password'
          },
          passwordValidator: {
            message: 'The password is not valid'
          }
        }
      }
    }
  })
  // on a change event we revalidate the password confirm so we avoid having
  // confirm as a success if we go back and change the password after confirm is
  // successful
  .on('change', '[name="password"]', function(e) {
    $('#register-form').formValidation('revalidateField', 'confirm');
  })
  // only show one validation error at a time
  .on('err.validator.fv', function(e, data) {
    // $(e.target)    --> The field element
    // data.fv        --> The FormValidation instance
    // data.field     --> The field name
    // data.element   --> The field element
    // data.validator --> The current validator name

    data.element
      .data('fv.messages')
      // Hide all the messages
      .find('.help-block[data-fv-for="' + data.field + '"]').hide()
      // Show only message associated with current validator
      .filter('[data-fv-validator="' + data.validator + '"]').show();
  })
  .on('success.form.fv', function(e) {
    // Prevent form submission
    e.preventDefault();

    var $form = $(e.target),
      fv = $form.data('formValidation'),
      url = API_URL + '/account/register',
      method = 'POST';

    // Use Ajax to submit form data
    $.ajax({
      url: url,
      type: method,
      data: $form.serialize(),
      beforeSend: function () {
        $('#register-spinner')[0].style.visibility = 'visible';
        $('#messages').html('');
      },
      success: function (result) {
        if (result === 'success') {
          $('#messages').html('<p class="help-block">Almost there...</p><p class="help-block">An activation email was sent to you, open it to continue.</p>');
        } else {
          //$('#messages').html('<small class="help-block" >The registration details submitted were not valid</small>');
          var errors = result.reduce(function(a, b) {
              return a + '<li><small class="help-block">' + b + '</small></li>';
            }, '');
          $('#messages').html('<ul class="has-error">' + errors + '</ul>');
        }
      },
      complete: function (result) {
        $('#register-spinner')[0].style.visibility = 'hidden';
      },
      error: function(result) {
        window.location = 'errors/connection-error.html';
      }
    });
  });

  $('#password-reset-step1').formValidation({
    // validating Bootstrap form
    framework: 'bootstrap',

    // Feedback icons
    icon: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },

    // List of fields and their validation rules
    fields: {
      email: {
        validators: {
          notEmpty: {
            message: 'Please enter your email address'
          },
          emailAddress: {
            message: 'That is not a valid email address'
          }
        }
      }
    }
  })
  // for the sign in we don't show the success visuals since everything
  // is a success other than having a blank input
  .on('success.field.fv', function(e, data) {
    var $parent = data.element.parents('.form-group');

    // Remove the has-success class
    $parent.removeClass('has-success');

    // Hide the success icon
    data.element.data('fv.icon').hide();
  })
  .on('success.form.fv', function(e) {
    // Prevent form submission
    e.preventDefault();

    var $form = $(e.target),
      fv = $form.data('formValidation'),
      url = API_URL + '/account/refresh-password-verification-token',
      method = 'POST';

    var $messages = $('#password-reset-step1 .messages');

    // Use Ajax to submit form data
    $.ajax({
      url: url,
      type: method,
      data: $form.serialize(),
      beforeSend: function() {
        $('#forgot-password-step1-spinner')[0].style.visibility = 'visible';
        $messages.html('');
        $messages.removeClass('has-error');
        console.log($form.serialize());
      },
      success: function(result) {
        if (result === 'success') {
          $messages.html('<small class="help-block">An email containing a password reset token was sent to you. Copy the token and paste it into Step 2.</small>');
        } else {
          $messages.addClass('has-error');
          $messages.html('<small class="help-block">' + result + '</small>');
        }
      },
      complete: function(result) {
        $('#forgot-password-step1-spinner')[0].style.visibility = 'hidden';
      },
      error: function(result) {
        $messages.addClass('has-error');
        $messages.html('<small class="help-block">An unexpected error occured.</small>');
      }
    });
  });

  $('#password-reset-step2').formValidation({
    // validating Bootstrap form
    framework: 'bootstrap',

    // Feedback icons
    icon: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },

    // List of fields and their validation rules
    fields: {
      token: {
        validators: {
          notEmpty: {
            message: 'Please enter the password reset token you received following Step 1'
          },
          stringLength: {
              min: 36,
              max: 36,
              message: 'The token must be exactly 36 characters long and has the form XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX. Make sure there is no leading or trailing spaces'
          },
        }
      },
      password: {
        validators: {
          notEmpty: {
            message: 'Please enter your password'
          },
          passwordValidator: {
            message: 'The password is not valid'
          }
        }
      }
    }
  })
  // for the sign in we don't show the success visuals since everything
  // is a success other than having a blank input
  .on('success.field.fv', function(e, data) {
    var $parent = data.element.parents('.form-group');

    // Remove the has-success class
    $parent.removeClass('has-success');

    // Hide the success icon
    data.element.data('fv.icon').hide();
  })
  .on('success.form.fv', function(e) {
    // Prevent form submission
    e.preventDefault();

    var $form = $(e.target),
      fv = $form.data('formValidation'),
      url = API_URL + '/account/update-password',
      method = 'POST';

    var $messages = $('#password-reset-step2 .messages');

    // Use Ajax to submit form data
    $.ajax({
      url: url,
      type: method,
      data: $form.serialize(),
      beforeSend: function() {
        $('#forgot-password-step2-spinner')[0].style.visibility = 'visible';
        $messages.html('');
        $messages.removeClass('has-error');
        console.log($form.serialize());
      },
      success: function(result) {
        if (result === 'success') {
          $messages.html('<small class="help-block">Your password was successfully updated.</small>');
        } else {
          $messages.addClass('has-error');
          $messages.html('<small class="help-block">' + result + '</small>');
        }
      },
      complete: function(result) {
        $('#forgot-password-step2-spinner')[0].style.visibility = 'hidden';
      },
      error: function(result) {
        $messages.addClass('has-error');
        $messages.html('<small class="help-block">An unexpected error occured.</small>');
      }
    });
  });

});

(function($) {
  FormValidation.Validator.passwordValidator = {
    validate: function(validator, $field, options) {
      var value = $field.val();
      var reqLength = 7;

      // Check the password strength
      if (value.length < reqLength) {
        return {
          valid: false,
          message: 'The password must be at least ' + reqLength + ' characters long'
        };
      }

      return true;
    }
  };
}(window.jQuery));
