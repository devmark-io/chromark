/**
 * Authenticates a user
 *
 * @param {string} username - The _login of the user to be
 * authenticated
 *
 * @param {string} password - The _password of the user to be
 * authenticated
 *
 * @param {function(object)} onSuccess - Called after authentication
 * is successful
 *
 * @param {function(object)} onFailure - Called if the attempt
 * at authentication fails
 *
 * @param {function(object)} onError - Called when there
 * is an error during authentication
 *
 **/
function signIn(username, password, onSuccess, onFailure, onError) {

  // TODO: should validate _login and _password here

  var url = API_URL + '/account/signin';
  var x = new XMLHttpRequest();

  x.open('POST', url);
  x.responseType = 'text';

  var params = {
    username: username.value,
    password: password.value
  };

  x.setRequestHeader('Content-type', 'application/json');

  x.onload = function() {
    if(x.status !== 200) {
      onFailure();
    } else {
      onSuccess();
    }
  };

  x.onerror = function() {
    onError(x.error);
  };

  x.send(JSON.stringify(params));

}

/**
 * Unauthenticates a user
 *
 * @param {function(object)} onSuccess - Called after unauthentication
 * is successful
 *
 * @param {function(object)} onFailure - Called if the attempt
 * at unauthentication fails
 *
 * @param {function(object)} onError - Called when there
 * is an error during unauthentication
 *
 **/
function signOut(onSuccess, onFailure, onError) {

  var url = API_URL + '/account/signout';
  var x = new XMLHttpRequest();

  x.open('POST', url);
  x.responseType = 'text';

  x.onload = function() {
    if(x.status !== 200) {
      onFailure();
    } else {
      onSuccess();
    }
  };

  x.onerror = function() {
    onError(x.error);
  };

  x.send();

}

/**
 * Checks if a mark exists in the DB
 *
 * @param {string} markUrl - called when the URL of the current tab
 *   is found.
 *
 * @param {function(boolean)} onComplete - called when the boolean indicating
 *   if the mark exists is returned.
 *
 * @param {function(string)} onError - called if there is an issue when
 *   performing the call to the server.
 **/
function markExists(markUrl, onComplete, onError) {

  var url = API_URL + '/mark/read/url/' + encodeURIComponent(markUrl);
  var x = new XMLHttpRequest();

  x.open('GET', url);
  x.responseType = 'json';

  x.onload = function() {
    if (x.status === 401) {
      window.location = 'signin.html';
    }

    onComplete(x.response.id > 0, x.response);
  };

  x.onerror = function() {
    onError('Network error.');
  };

  x.send();

}

/**
 * Gets the pinned marks from DB
 *
 * @param {function(object)} onComplete - Called when the pinned marks
 *    are return from the DB.
 *
 * @param {function(string)} onError - called if there is an issue when
 *   performing the call to the server.
 **/
function getPinned(onComplete, onError) {

  var url = API_URL + '/mark/pinned';
  var x = new XMLHttpRequest();

  x.open('GET', url);
  x.responseType = 'json';

  x.onload = function() {

    if (x.status === 401) {
      window.location = 'signin.html';
    }

    if (!x.response) return;

    onComplete(x.response);
  };

  x.onerror = function() {
    window.location = "errors/connection-error.html";
  };

  x.send();
}

/**
 * Deletes a mark from the DB
 *
 * @param {function(object)} onComplete - Called when the mark is
 *    deleted from the DB.
 *
 * @param {function(string)} onError - called if there is an issue when
 *   performing the call to the server.
 **/
function deleteMark(id, onComplete, onError) {

  var x = new XMLHttpRequest();

  x.open('DELETE', API_URL + '/mark/delete/'+ id);

  x.onload = function() {
    onComplete();
  };

  x.onerror = function() {
    onError('Network error.');
  };

  x.send();
}

/**
 * Updates a mark that already exists in the DB
 *
 * @param {function(object)} onComplete - Called when the mark is
 *    updated in the DB.
 *
 * @param {function(string)} onError - called if there is an issue when
 *   performing the call to the server.
 **/
function updateMark(mark, onComplete, onError) {

  var url = API_URL + '/mark/update/'+ mark.id;
  var x = new XMLHttpRequest();

  x.open('PUT', url);
  x.setRequestHeader('Content-type', 'application/json');

  x.onload = function() {
    onComplete();
  };

  x.onerror = function() {
    onError('Network error.');
  };

  x.send(JSON.stringify(mark));
}

/**
 * Creates a new mark in the DB from the url of whatever tab is opened
 *
 * @param {object} mark - The object containing the data to create
 * the new mark in the DB.
 *
 * @param {function(object)} onComplete - Called when the mark is
 *    created in the DB.
 *
 * @param {function(string)} onError - called if there is an issue when
 *   performing the call to the server.
 **/
function createMark(mark, onComplete, onError) {

  // TODO: should validate mark at this point

  var url = API_URL + '/mark/create';
  var x = new XMLHttpRequest();

  x.open('POST', url);
  x.setRequestHeader('Content-type', 'application/json');

  x.onload = function() {
    onComplete(JSON.parse(x.response));
  };

  x.onerror = function() {
    onError('Network error.');
  };

  x.send(JSON.stringify(mark));
}

/**
 * Searches the DB for marks related to the query
 *
 * @param {string} query - Query string used to search for marks
 *
 * @param {function(object)} onComplete - Called when the results of
 *    the DB query is returned.
 *
 * @param {function(string)} onError - called if there is an issue when
 *   performing the call to the server.
 **/
function searchMarks(query, onComplete, onError) {

  // TODO: should validate query at this point

  var url = API_URL + '/mark/search/'+ encodeURIComponent(query);
  var x = new XMLHttpRequest();

  x.open('GET', url);
  x.responseType = 'json';

  x.onload = function() {
    if (!x.response) return;
    onComplete(x.response);
  };

  x.onerror = function() {
    onError('Network error.');
  };

  x.send();

}
