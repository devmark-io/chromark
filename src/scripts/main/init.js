// shorthands for common elements on the interface
var $shortcuts, $add, $save, $edit, $editClass, $deleteClass, $deleteFav, $deleteName, $deleteUrl, $editFav, $editContainer, $editCancel, $editUpdate, $editName,
    $editUrl, $editPinned, $editShortcut, $search, $searchBtn, $mode, $allLinks, $commands, $searchResults,
    $searchResult, $currentTab, $footer, $searchSpinner, $delete, $change, $gotoEdit, $gotoDelete;

if (!chrome.cookies) {
  chrome.cookies = chrome.experimental.cookies;
}

document.addEventListener('DOMContentLoaded', function() {

  $shortcuts = document.getElementById('shortcuts');
  $add = document.getElementById('add');
  $save = document.getElementById('save');
  $edit = document.getElementById('edit');
  $editClass = document.getElementsByClassName('edit');
  $deleteClass = document.getElementsByClassName('delete');
  $deleteFav = document.getElementById('delete-favicon');
  $deleteName = document.getElementById('delete-name');
  $deleteUrl = document.getElementById('delete-url');
  $editFav = document.getElementById('edit-favicon');
  $editContainer = document.getElementById('edit-container');
  $editName = document.getElementById('edit-name');
  $editUrl = document.getElementById('edit-url');
  $editPinned = document.getElementById('edit-pinned');
  $editShortcut = document.getElementById('edit-shortcut');
  $editUpdate = document.getElementById('edit-update');
  $editCancel = document.getElementById('edit-cancel');
  $search = document.getElementById('search');
  $searchBtn = document.getElementById('search-btn');
  $mode = document.getElementById('mode');
  $allLinks = document.getElementsByTagName('a');
  $commands = document.getElementsByClassName('command');
  $searchResults = document.getElementById('search-results');
  $searchResult = document.getElementsByClassName('search-result');
  $currentTab = document.getElementById('current-tab');
  $footer = document.getElementsByTagName('footer')[0];
  $searchSpinner = document.getElementById('search-spinner');
  //$footerSpinner = document.getElementById('footer-spinner');
  $delete = document.getElementById('delete');
  $change = document.getElementById('change');
  $gotoEdit = document.getElementById('goto-edit');
  $gotoDelete = document.getElementById('goto-delete');

  chrome.cookies.get({ "name": "koa.sid", "url": API_URL }, function(cookies) {
    bindActionsToEvents();

    if(!cookies) {
      window.location = 'signin.html';
    } else {
      init();
    }
  });

});

