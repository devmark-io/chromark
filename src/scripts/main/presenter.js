/**
 * Calls the showEditButton function and then forces
 * focus on the edit button
 *
 **/
function showAndFocusEditButton(mark) {
  showEditButton(mark);
  $edit.focus();
}

/**
 * Shows the edit button and hides the save button
 *
 **/
function showEditButton(mark) {
  $edit.dataset.id = mark.id;
  $edit.dataset.name = mark.name;
  $edit.dataset.url = mark.url;
  $edit.dataset.pinned = mark.pinned;
  $edit.dataset.shortcut = mark.shortcut;
  $edit.dataset.favicon = mark.favicon;

  $edit.disabled = false;
  $edit.style.display = 'inline-block';

  $save.style.display = 'none';
}

/**
 * Shows the save button and hides the edit button
 *
 **/
function showSaveButton() {
  $save.disabled = false;
  $save.style.display = 'inline-block';
  $save.innerText = 'save';

  $edit.style.display = 'none';
}

/**
 * Applies the disabled CSS class to the save button
 *
 **/
function disableSaveButton() {
  $save.disabled = true;
  addClass($save, ' disabled');
}

/**
 * Renders the search results
 *
 * @param {object} marks - A json object containing one or
 *    more mark objects.
 *
 **/
function renderSearchResults(marks, focusIndex) {
  // wipe any existing search results
  $searchResults.innerHTML = '';

  // if no marks are found the search result are just left empty
  if (marks.length === 0) {
    return;
  }


  var callback = function(result) {
    $searchResults.appendChild(result);
  };

  marks.forEach( function(mark) {
    buildSearchResult(mark, callback);
  });

  if (focusIndex > -1) {
    var i = $searchResult.length - 1;
    $searchResult[focusIndex].focus();
  }
}

/**
 * Renders the pinned marks
 *
 * @param {object} marks - A json object containing one or
 *    more mark objects.
 *
 **/
function renderPinned(marks) {

  // ensure the shortcut wipes any text content in might have
  $shortcuts.textContent = '';

  var callback = function(shortcut) {
    $shortcuts.appendChild(shortcut);
  };

  marks.forEach( function(mark) {
    buildShortcut(mark, callback);
  });
}
