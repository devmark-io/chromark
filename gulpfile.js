const gulp = require('gulp'),
      rev = require('gulp-rev'),
      es = require('event-stream'),
      zip = require('gulp-zip'),
      cleanCss = require('gulp-clean-css'),
      uglify = require('gulp-uglify'),
      del = require('del'),
      rename = require('gulp-rename');


var paths = {
  scripts: ['src/scripts/**/*.js'],
  styles: ['src/styles/**/*.css'],
  views: ['src/views/**/*.html'],
  images: ['src/images/**/*']
};

gulp.task('watch', function() {
  gulp.watch(paths.scripts, ['dev-build']);
  gulp.watch(paths.styles, ['dev-build']);
  gulp.watch(paths.views, ['dev-build']);
  gulp.watch(paths.images, ['dev-build']);
});

gulp.task('clean-tmp', function () {
  return del(['tmp/*']);
});

gulp.task('clean-target', function () {
  return del(['target/*']);
});

gulp.task('clean-publish', function () {
  return del(['chromark-*.zip']);
});

gulp.task('pre-dev-build', ['clean-tmp', 'clean-target'], function () {
  var vendors = gulp.src('vendor/**/*')
    .pipe(gulp.dest('tmp/vendor'));

  var styles = gulp.src('src/styles/*.css')
    .pipe(gulp.dest('tmp/styles'));

  var scripts = gulp.src('src/scripts/**/*.js')
    .pipe(gulp.dest('tmp/scripts'));

  var html = gulp.src('src/views/**/*.html')
    .pipe(gulp.dest('tmp/views'));

  var logo = gulp.src(['src/images/logo.png'])
    .pipe(gulp.dest('tmp/images'));

  var icon = gulp.src(['src/images/dev_icon.png'])
    .pipe(rename('icon.png'))
    .pipe(gulp.dest('tmp/images/'));

  var manifest = gulp.src(['manifest.dev.json'])
    .pipe(rename('manifest.json'))
    .pipe(gulp.dest('tmp/'));

  return es.merge(vendors, styles, scripts, html, logo, icon, manifest);
});

gulp.task('dev-build', ['watch', 'pre-dev-build'], function () {
  return gulp.src(['tmp/**/*'])
    .pipe(gulp.dest('target/'));
});

gulp.task('publish-build', ['clean-tmp', 'clean-publish'], function () {
  var vendors = gulp.src('vendor/**/*')
    .pipe(gulp.dest('tmp/vendor'));

  var styles = gulp.src('src/styles/*.css')
    .pipe(cleanCss())
    .pipe(gulp.dest('tmp/styles'));

  var scripts = gulp.src('src/scripts/**/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('tmp/scripts'));

  var html = gulp.src('src/views/**/*.html')
    .pipe(gulp.dest('tmp/views'));

  var logo = gulp.src(['src/images/logo.png'])
    .pipe(gulp.dest('tmp/images'));

  var icon = gulp.src(['src/images/icon.png'])
    .pipe(rename('icon.png'))
    .pipe(gulp.dest('tmp/images/'));

  var manifest = gulp.src(['manifest.prod.json'])
    .pipe(rename('manifest.json'))
    .pipe(gulp.dest('tmp/'));

  return es.merge(vendors, styles, scripts, html, logo, icon, manifest);
});

gulp.task('publish', ['publish-build'], function () {
  return gulp.src('tmp/**/*')
    .pipe(zip('chromark.zip'))
    .pipe(rev())
    .pipe(gulp.dest(''));
});

gulp.task('noop', function(){});
